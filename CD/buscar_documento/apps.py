from django.apps import AppConfig


class BuscarDocumentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buscar_documento'
