from django.apps import AppConfig


class AsignarEntrenamientoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'asignar_entrenamiento'
