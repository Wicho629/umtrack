from django.apps import AppConfig


class SolicitarFirmasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'solicitar_firmas'
