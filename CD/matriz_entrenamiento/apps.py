from django.apps import AppConfig


class MatrizEntrenamientoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'matriz_entrenamiento'
