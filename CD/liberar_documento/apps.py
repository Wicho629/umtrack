from django.apps import AppConfig


class LiberarDocumentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'liberar_documento'
