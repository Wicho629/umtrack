from django.apps import AppConfig


class BloquearDocumentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bloquear_documento'
