# Generated by Django 5.0.2 on 2024-03-16 05:53

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('area', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Linea',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_linea', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='UnidadNegocio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unidad_negocio', models.CharField(max_length=100)),
                ('gerente_Calidad', models.IntegerField()),
                ('gerente_ingenieria', models.IntegerField()),
                ('gerente_produccion', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('departamento', models.CharField(max_length=30)),
                ('id_area', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Usuarios.area')),
            ],
        ),
        migrations.CreateModel(
            name='PerfilUsuario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no_empleado', models.CharField(max_length=20)),
                ('estado', models.CharField(max_length=10)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Puesto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion_general', models.CharField(max_length=100)),
                ('tipo', models.CharField(max_length=20)),
                ('por_unidad_negocio', models.BooleanField()),
                ('id_departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Usuarios.departamento')),
            ],
        ),
        migrations.CreateModel(
            name='UsuarioPuesto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puesto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Usuarios.puesto')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('usuario', 'puesto')},
            },
        ),
    ]
