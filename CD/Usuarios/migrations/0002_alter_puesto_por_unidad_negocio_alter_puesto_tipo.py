# Generated by Django 5.0.2 on 2024-03-20 05:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puesto',
            name='por_unidad_negocio',
            field=models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E')], max_length=40),
        ),
        migrations.AlterField(
            model_name='puesto',
            name='tipo',
            field=models.CharField(choices=[('Administrador', 'Administrador'), ('Control Documentos', 'Control Documentos'), ('Entrenamiento', 'Entrenamiento'), ('Empleado', 'Empleado')], max_length=40),
        ),
    ]
