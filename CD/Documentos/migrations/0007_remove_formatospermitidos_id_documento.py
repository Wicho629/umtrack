# Generated by Django 5.0.2 on 2024-04-07 05:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Documentos', '0006_remove_documento_revision_plantilla_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formatospermitidos',
            name='id_documento',
        ),
    ]
