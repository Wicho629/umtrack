from django.apps import AppConfig


class AgregarDibujoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'agregar_dibujo'
