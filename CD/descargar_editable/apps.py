from django.apps import AppConfig


class DescargarEditableConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'descargar_editable'
