from django.apps import AppConfig


class AprobarDocumentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aprobar_documento'
