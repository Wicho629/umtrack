from django.apps import AppConfig


class ActualizarDocumentoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'actualizar_documento'
